package com.boomi.connector.DemoAPI;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

public class DemoAPIConnector extends BaseConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new DemoAPIBrowser(createConnection(context));
    }    

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return new DemoAPIGetOperation(createConnection(context));
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new DemoAPIExecuteOperation(createConnection(context));
    }
   
    private DemoAPIConnection createConnection(BrowseContext context) {
        return new DemoAPIConnection(context);
    }
}