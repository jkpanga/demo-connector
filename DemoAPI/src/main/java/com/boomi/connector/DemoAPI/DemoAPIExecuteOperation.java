package com.boomi.connector.DemoAPI;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;

public class DemoAPIExecuteOperation extends BaseUpdateOperation {

	protected DemoAPIExecuteOperation(DemoAPIConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public DemoAPIConnection getConnection() {
        return (DemoAPIConnection) super.getConnection();
    }
}