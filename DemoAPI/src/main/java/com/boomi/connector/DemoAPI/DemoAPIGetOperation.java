package com.boomi.connector.DemoAPI;

import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.util.BaseGetOperation;

public class DemoAPIGetOperation extends BaseGetOperation {

    protected DemoAPIGetOperation(DemoAPIConnection conn) {
        super(conn);
    }

	@Override
	protected void executeGet(GetRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public DemoAPIConnection getConnection() {
        return (DemoAPIConnection) super.getConnection();
    }
}